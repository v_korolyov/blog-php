<?php
if(isset($_POST['name']) && isset($_POST['comment'])) {
    $success = false;
    $err = [];

    $name = trim($_POST['name']);
    $name = strip_tags($name);
    $name = htmlspecialchars($name);

    $comment = trim($_POST['comment']);
    $comment = strip_tags($comment);
    $comment = htmlspecialchars($comment);

// проверки

    if(empty($name)) {
        $err[] = "Укажите имя.";
    }
    if(empty($comment)) {
        $err[] = "Заполните текст отзыва.";
    }
    if(empty($err)) {
        $success = true;
    }

    echo json_encode(array($success, $err, $name, $comment));
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Гостевая книга</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="jquery-3.3.1.js"></script>
<!--    <script src="./assets/js/func.js"></script>-->

</head>
<body>

    <script type="application/javascript">
        $(function() {
            $("#submit-guest-comment").click(function(event) {
                event.preventDefault();
                var gcomment = $('#form-guest-comment').serialize();
                console.log(gcomment);
                $.ajax({
                    type: 'POST',
                    url: 'guestbook.php',
                    data: gcomment,
                    success: function (data) {
                        console.log(data);
                        if(!data[0] && data[1].length > 0) {
                            var alertnotice = '';
                            data[1].forEach(function(notice) {
                                alertnotice += notice+"<br>";
                            });
                            // $('#form-guest-comment').before("<div class='gb-alert alert alert-warning col-md-4 col-md-pull-4' role='alert'>" + alertnotice + "</div>");
                            // setTimeout(function() { $(".gb-alert").remove(); }, 3000);

                            $('#alert_message').html(alertnotice);
                            $("#alert_box").fadeIn(500).delay(3000).fadeOut(500);

                        } else {
                            var gbcomment = "<div class='container-md'>" +
                                "<hr><span class='display-4'>Имя: "+data[2]+"</span>\n" +
                                "<p class='h3'>Отзыв: "+data[3]+"</p>\n" +
                                "</div>";
                            $('#guest-comment').prepend(gbcomment);
                        }
                    },
                    dataType: 'json'
                });
            });
        });
    </script>

    <div id='alert_box' style='display: none; border: 1px solid white; background: #ffebbe; position: fixed; width: 370px; right: 20px; top: 20px; box-shadow: 0px 5px 5px 3px rgba(0, 0, 0, 0.3); border-radius: 5px; text-align: center; z-index: 1'>
        <p id='alert_message' style='color: #856404;; margin-top: 18px; font-weight: bold;  text-shadow: 1px 1px 1px #FFFFFF; filter: dropshadow(color=#FFFFFF, offx=1, offy=1);'></p>
    </div>

    <div class="d-flex align-items-center flex-column">
        <form id="form-guest-comment" class="col-lg-6">
            <div class="form-group"><input class="form-control form-control-lg" type="text" name="name" placeholder="Ваше имя" required>
            </div>
            <div class="form-group">
                <textarea class="form-control input-group-prepend form-control-lg" name="comment" placeholder="Отзыв" required ></textarea></div>
            <div class="form-group">
                <input class="btn-primary btn-lg btn-block" type="submit" id="submit-guest-comment" value="Отправить"></div>
        </form>
    </div>

    <div id="guest-comment" class="d-flex align-items-center flex-column"></div>

</body>
</html>
